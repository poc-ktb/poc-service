def mvnCmd = "mvn -s artifact_setting.xml" 
def imageNamespace = "ci-cd"
def getVersionFromPom(pom) {
  def matcher = readFile(pom) =~ '<version>(.+)</version>'
  matcher ? matcher[0][1] : null
}
def version
pipeline {
  agent {
      label 'maven-with-tools'
      //label 'maven'
      // registry.redhat.io/openshift3/jenkins-agent-maven-35-rhel7:v3.11
  }
  stages {
  	
    stage('Pull Code') {
      steps {
        git branch: 'master', url: 'https://gitlab.com/poc-ktb/poc-service.git'
        script {
            version = getVersionFromPom("pom.xml");            
        }
      }
    }
    stage('Build App'){
      steps {
        echo "Build App"
        sh "${mvnCmd} package -DskipTests=true" 

        echo "Push binary to Registry" 
        sh "${mvnCmd} deploy -DskipTests=true -DaltDeploymentRepository=central::default::http://ec2-52-53-39-8.us-west-1.compute.amazonaws.com:8081/artifactory/libs-release/"
      }
    }
    stage('Unit Test'){
      steps {
        sh "${mvnCmd} test"
      }
    }
    stage('Scan Code'){
      steps {
        sh "echo sonar-scanner ..."
        withSonarQubeEnv('sonar-aws') {
            //sh '${mvnCmd} sonar:sonar'
            sh 'mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.2:sonar'
        }
        sh "echo check for result"
        sleep(10)
        waitForQualityGate abortPipeline: true        
      }
    }
    
    stage('Pack Container'){
      when{
        expression{
          openshift.withCluster(){
            openshift.withProject(env.CICD_PROJECT){
              return !openshift.selector("bc","poc-service").exists()
            }
          }
        }
      }
      steps {
        script {
          openshift.withCluster(){
            openshift.withProject(env.CICD_PROJECT){
              openshift.newBuild("--name=poc-service", "--to=ci-cd/poc-service:${version}","--image-stream=redhat-openjdk18-openshift:1.4", "--binary=true")
              openshift.startBuild("poc-service", "--from-file=target/poc-service-${version}.jar", "--wait")
            }
          }
        }
      }
    }
    stage('Scan Image'){
      steps{
        // need skopeo to copy image to Artifactory?
        sh "echo scan image"
        sh "echo check for result"

        //Teg Images to latest
        script {
          openshift.withCluster() {
            openshift.withProject(env.CICD_PROJECT) {
              openshift.tag("poc-service:${version}", "poc-service:latest")
            }
          }
        }
      }
    }
    stage('Push to Registry') {
      steps{
        //push images to artifactory
        sh "echo Push to Registry"

      }
    }
    stage('Create DeploymentConfig'){
      when{
        expression{
          openshift.withCluster(){
            openshift.withProject(env.DEV_PROJECT){
              return !openshift.selector("dc","poc-service").exists()
            }
          }
        }
      }
      steps {
        script {
          openshift.withCluster() {
            openshift.withProject(env.DEV_PROJECT) {
              def template = 'https://gitlab.com/poc-ktb/poc-service/raw/master/poc-service-dc-template.yaml'
              openshift.apply(
                openshift.process("-f", template, "-p", "IMAGE_VERSION=latest", "-p", "IMAGE_NAMESPACE=${imageNamespace}")
              )
            }
          }
        }
      }
    }
    stage('Create Route and SVC'){
      when{
        expression{
          openshift.withCluster(){
            openshift.withProject(env.DEV_PROJECT){
              return !openshift.selector("svc","poc-service").exists()
            }
          }
        }
      }
      steps {
        script {
          openshift.withCluster() {
            openshift.withProject(env.DEV_PROJECT) {
              def template = 'https://gitlab.com/poc-ktb/poc-service/raw/master/poc-service-svc-template.yaml'
              openshift.apply(
                openshift.process("-f", template)
              )
            }
          }
        }
      }
    }
    
    stage('Deploy to STG') {
        steps {
            //disable tr
            openshiftDeploy depCfg: 'poc-service', namespace: "${DEV_PROJECT}", verbose: 'false', waitTime: '', waitUnit: 'sec'
            openshiftVerifyDeployment depCfg: 'poc-service', namespace: "${DEV_PROJECT}", replicaCount: '1', verbose: 'false', verifyReplicaCount: 'false', waitTime: '', waitUnit: 'sec'
        	  openshiftVerifyService namespace: "${DEV_PROJECT}", svcName: 'poc-service', verbose: 'false'
        }
    }
    
    stage('Automate Testing') {
    	steps {
	        echo "Automate Testing"
	        
	        echo "JMeter Testing"
	        //sh "jmeter -Jjmeter.save.saveservice.output_format=xml -n -t performance_test/poc-service.jmx -l poc-service.jtl"
	        //perfReport(sourceDataFiles: 'poc-service.jtl', compareBuildPrevious: true, errorFailedThreshold: 10, errorUnstableResponseTimeThreshold: '50', errorUnstableThreshold: 50)
			    //perfReport(sourceDataFiles: 'poc-service.jtl')
	        
	        //echo "Robot FW Testing"
	        //sh "robot poc-service.robot"
        }
    }
    
    stage('Release Approval') {
      steps {
        script {
          openshift.withCluster() {
            def tags = openshift.selector("istag").objects().collect { it.metadata.name }.findAll { it.startsWith 'poc-service:' }.collect { it.replaceAll(/poc-service:(.*)/, "\$1") }.sort()
            timeout(5) {
              releaseTag = input(
                ok: "Deploy",
                message: "Enter release version to promote to PROD",
                parameters: [
                  choice(choices: tags.join('\n'), description: '', name: 'Release Version')
                ]
              )
            }
          }
          sh "echo releaseTag = ${releaseTag}"
        }
      }
    }
    stage('Deploy') {
      steps {
        withCredentials([usernamePassword(credentialsId: 'jboss-aws', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
          sh "/opt/jboss/bin/jboss-cli.sh --connect --controller=ec2-13-52-50-90.us-west-1.compute.amazonaws.com:9990 --user=${USERNAME} --password=${PASSWORD} --command='deploy target/poc-service-${version}.jar' "
        }
      }
    }
  }
}
