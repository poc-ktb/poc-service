package com.yip.labs.pocservice.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yip.labs.pocservice.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
}