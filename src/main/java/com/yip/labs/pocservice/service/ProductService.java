package com.yip.labs.pocservice.service;

import java.util.List;
import java.util.stream.Collectors;

import com.yip.labs.pocservice.dto.ProductDto;
import com.yip.labs.pocservice.entity.Product;
import com.yip.labs.pocservice.entity.ProductStatus;
import com.yip.labs.pocservice.persistence.ProductRepository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
@Service
@Transactional
public class ProductService {
	private final ProductRepository productRepository;

	public List<ProductDto> findAll() {
		log.debug("Request to get all Products");
		return this.productRepository.findAll().stream().map(ProductService::mapToDto).collect(Collectors.toList());
	}
	
	@Transactional(readOnly = true)
	public ProductDto findById(Long id) {
		log.debug("Request to get Product : {}", id);
		return this.productRepository.findById(id).map(ProductService::mapToDto).orElse(null);
	}
	
	ProductDto getDefaultProductId(Long id) {
		log.debug("getDefaultProductId", id);
		ProductDto productDto = new ProductDto();
		productDto.setId(id);
		productDto.setName("UNKNOW");
		productDto.setDescription("NONE");
		return productDto;
	}

	public ProductDto create(ProductDto productDto) {
		log.debug("Request to create Product : {}", productDto);
		return mapToDto(
				this.productRepository.save(
					new Product(
							productDto.getId(),
							productDto.getName(), 
							productDto.getDescription(),
							productDto.getPrice(), 
							productDto.getQuantity(), 
							ProductStatus.valueOf(productDto.getStatus()),
							productDto.getSalesCounter()
							)));
	}

	public void delete(Long id) {
		log.debug("Request to delete Product : {}", id);
		this.productRepository.deleteById(id);
	}

	public static ProductDto mapToDto(Product product) {
		log.debug("::ProductDto mapToDto product  ::"+product);
		if (product != null) {
			return new ProductDto(
					product.getId(), 
					product.getName(), 
					product.getDescription(), 
					product.getPrice(),
					product.getQuantity(), 
					product.getStatus().name(), 
					product.getSalesCounter()
					);
		}
		return null;
	}
}
