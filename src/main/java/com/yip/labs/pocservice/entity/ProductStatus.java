package com.yip.labs.pocservice.entity;

public enum ProductStatus {
	AVAILABLE, DISCONTINUED
}
