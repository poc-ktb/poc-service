package com.yip.labs.pocservice;

import java.math.BigDecimal;

import com.yip.labs.pocservice.entity.Product;
import com.yip.labs.pocservice.entity.ProductStatus;
import com.yip.labs.pocservice.persistence.ProductRepository;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class POCServiceApplication {


	public static void main(String[] args) {
		SpringApplication.run(POCServiceApplication.class, args);
	}

	@Bean
	public CommandLineRunner initialSuspensionData(ProductRepository productRepository) {
		return (args) -> {
			Product product = new Product(1L, "Prodcut_Name1", "Description_Prodcut_Name1", new BigDecimal("1000.50"), 5, ProductStatus.AVAILABLE, 0);
			productRepository.save(product);
			product = new Product(2L, "Prodcut_Name2", "Description_Prodcut_Name3", new BigDecimal("2000.50"), 5, ProductStatus.AVAILABLE, 0);
			productRepository.save(product);
			product = new Product(3L, "Prodcut_Name3", "Description_Prodcut_Name3", new BigDecimal("3000.50"), 5, ProductStatus.AVAILABLE, 0);
			productRepository.save(product);
		};
	}

}
