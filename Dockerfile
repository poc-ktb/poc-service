FROM openjdk:8u171-jdk-alpine3.8
ADD ./target/poc-service-0.0.1-RELEASE.jar poc-service-0.0.1-RELEASE.jar
EXPOSE 8091
ENTRYPOINT ["java","-jar","poc-service-0.0.1-RELEASE.jar"]
